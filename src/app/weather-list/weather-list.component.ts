import {Component, Input, OnInit} from '@angular/core';
import {SearchService} from '../core/services/search.service';
import weatherData from '../core/services/weatherdata.json';

@Component({
  selector: 'app-weather-list',
  templateUrl: './weather-list.component.html',
  styleUrls: ['./weather-list.component.scss']
})
export class WeatherListComponent implements OnInit {

  @Input()
  topic: string;
  stackAndWeatherList = [];

  constructor(private searchService: SearchService) { }

  ngOnInit() {
    this.stackAndWeatherList = [];
    this.searchService.search(this.topic)
      .subscribe(weather => {
      const stackData = weather ['items'];
    for (let i = 0; i < 10; i++) {
      if (i % 2 === 0) {
        this.stackAndWeatherList.push(stackData[i / 2]);
      } else {
        this.stackAndWeatherList.push(weatherData[Math.floor(Math.random() * 4711)]);
      }
    }
    });
  }

}
