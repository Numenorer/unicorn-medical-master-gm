import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import {SearchService} from './core/services/search.service';
import {AppRoutingModule} from './app.routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import {LayoutModule} from './core/layout/layout.module';
import { SearchComponent } from './search/search.component';
import {HttpClientModule} from '@angular/common/http';
import {StackOverListComponent} from './stack-over-list/stack-over-list.component';
import { StackOverListItemComponent } from './stack-over-list/stack-over-list-item/stack-over-list-item.component';
import {MatListModule, MatTabsModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { WeatherListComponent } from './weather-list/weather-list.component';
import { WeatherListItemComponent } from './weather-list/weather-list-item/weather-list-item.component';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    SearchComponent,
    StackOverListComponent,
    StackOverListItemComponent,
    WeatherListComponent,
    WeatherListItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,

    LayoutModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatTabsModule,
  ],
  providers: [SearchService],
  bootstrap: [AppComponent]
})
export class AppModule { }
