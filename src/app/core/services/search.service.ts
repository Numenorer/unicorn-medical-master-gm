import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';


export interface ISearchResultItem {
  answer_count: number;
  closed_date: number;
  closed_reason: string;
  creation_date: number;
  is_answered: boolean;
  last_activity_date: number;
  link: string;
  score: number;
  tags: Array<string>;
  title: string;
  view_count: number;
}

@Injectable()
export class SearchService {

  private static readonly apiUrl =
    'https://api.stackexchange.com/2.2/search?pagesize=10&order=desc&sort=activity&site=stackoverflow&intitle=';

  constructor(private http: HttpClient) {

  }

  search(keyword: string): Observable<JSON> {
    return this.http.get(SearchService.apiUrl + keyword)
      .pipe(map((data: JSON) => {
        console.log('API USAGE: \' + data.quota_remaining +  \'of \' + data.quota_max + \' requests available');
        return data;
      })
    );
  }



}
