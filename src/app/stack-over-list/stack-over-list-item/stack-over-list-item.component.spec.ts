import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StackOverListItemComponent } from './stack-over-list-item.component';

describe('StackOverListItemComponent', () => {
  let component: StackOverListItemComponent;
  let fixture: ComponentFixture<StackOverListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StackOverListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StackOverListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
