import {Component, Input, OnInit} from '@angular/core';
import {StackOverItems} from '../../core/interfaces/stackOverItems';

@Component({
  selector: 'app-stack-over-list-item',
  templateUrl: './stack-over-list-item.component.html',
  styleUrls: ['./stack-over-list-item.component.scss']
})
export class StackOverListItemComponent implements OnInit {

  @Input()
  stackOverItem: StackOverItems;

  constructor() { }

  ngOnInit() {
  }

}
