import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StackOverListComponent } from './stack-over-list.component';

describe('StackOverListComponent', () => {
  let component: StackOverListComponent;
  let fixture: ComponentFixture<StackOverListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StackOverListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StackOverListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
