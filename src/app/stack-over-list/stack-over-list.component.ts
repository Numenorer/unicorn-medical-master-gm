import {Component, Input, OnInit} from '@angular/core';
import {StackOverItems} from '../core/interfaces/stackOverItems';
import {SearchService} from '../core/services/search.service';


@Component({
  selector: 'app-stack-over-list',
  templateUrl: './stack-over-list.component.html',
  styleUrls: ['./stack-over-list.component.scss']
})
export class StackOverListComponent implements OnInit {

  @Input()
  topic: string;
  stackOverListItems: StackOverItems;

  constructor(private searchService: SearchService) { }

  ngOnInit() {
    this.searchService.search(this.topic)
      .subscribe(data => this.stackOverListItems = data['items']);
  }

}
